﻿using DL.DataAccess;
using DL.WebUI.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.IO;

namespace DL.WebUI
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; private set; }
        public DbOptions DbOptions { get; set; }
        public ApplicationOptions ApplicationOptions { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("webui.settings\\settings.json");

            this.Configuration = builder.Build();
            this.DbOptions = Configuration.GetSection("Db").Get<DbOptions>();
            this.ApplicationOptions = Configuration.GetSection("Application").Get<ApplicationOptions>();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DlContext>(options =>
            {
                if (this.ApplicationOptions.Mode.Equals("dev"))
                {
                    options.UseSqlServer(this.DbOptions.DevConnectionString);
                }
                else
                {
                    options.UseSqlServer(this.DbOptions.ConnectionString);
                }

                options.ConfigureWarnings(configure => { configure.Throw(RelationalEventId.QueryClientEvaluationWarning); });
            });
            services.AddMvc(setup =>
            {
                
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }
    }
}
