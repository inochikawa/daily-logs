﻿namespace DL.WebUI.Options
{
    public class DbOptions
    {
        public string DevConnectionString { get; set; }
        public string ConnectionString { get; set; }
    }
}
