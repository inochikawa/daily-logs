﻿using System;

namespace DL.DataAccess.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
