﻿using System;

namespace DL.DataAccess.Entities
{
    public class Report : BaseEntity
    {
        public string HaveDoneYesterday { get; set; }
        public string HaveDoneToday { get; set; }
        public string Issues { get; set; }
        public DateTimeOffset ReportDate { get; set; }
    }
}
