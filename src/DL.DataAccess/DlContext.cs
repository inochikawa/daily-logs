﻿using DL.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DL.DataAccess
{
    public class DlContext: DbContext
    {
        public DlContext(DbContextOptions<DlContext> options) : base(options)
        {

        }


        public DbSet<Report> Reports { get; set; }
    }
}
